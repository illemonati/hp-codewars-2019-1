import readline, { ReadLine } from 'readline';

const main = async () => {
    const input = await getLine();
    const [h, m, s] = input.split(' ').map((s) => parseInt(s));
    if (s * h >= m) {
        console.log(`${input}. I will make it!`);
    } else {
        console.log(`${input}. I will be late!`);
    }
};

const getLine = (): Promise<string> => {
    const rl = readline.createInterface({
        input: process.stdin,
    });
    return new Promise((resolve) => {
        rl.on('line', (line) => {
            rl.close();
            resolve(line);
        });
    });
};

main().then();
