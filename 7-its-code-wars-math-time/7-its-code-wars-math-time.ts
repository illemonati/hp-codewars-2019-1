import readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
});

enum Operation {
    Even,
    Prime,
    Square,
    Cube,
}

const main = async () => {
    for await (const line of rl) {
        const lineArr = line.split(' ');
        const value = parseInt(lineArr[0]);
        const operation = parseInt(lineArr[1]) as Operation;
        if (operation === Operation.Even) {
            if (isEven(value)) {
                console.log(value + 2);
            } else {
                console.log(value + 1);
            }
        } else if (operation === Operation.Prime) {
            for (let i = value; true; i++) {
                if (isPrime(i)) {
                    console.log(i);
                    break;
                }
            }
        } else if (operation === Operation.Square) {
            const sqrt = Math.floor(Math.sqrt(value)) + 1;
            console.log(sqrt ** 2);
        } else {
            const cbrt = Math.floor(Math.cbrt(value)) + 1;
            console.log(cbrt ** 3);
        }
    }
};

const isPrime = (num: number): boolean => {
    for (let i = 2; i < num; ++i) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
};

const isEven = (num: number): boolean => (num & 1) === 0;

main().then();
