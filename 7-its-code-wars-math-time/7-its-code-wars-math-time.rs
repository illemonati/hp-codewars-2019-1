use std::io::{self, BufRead};

#[derive(PartialEq)]
enum Operation {
    Even,
    Prime,
    Square,
    Cube,
}

macro_rules! is_even {
    ($x: expr) => {
        ($x & 1) == 0
    };
}

fn is_prime(val: usize) -> bool {
    for i in 2..val {
        if (val % i) == 0 {
            return false;
        }
    }
    true
}

fn main() {
    for line in io::stdin().lock().lines() {
        let line = line.unwrap();
        let line_arr: Vec<&str> = line.split(' ').collect();
        let value: usize = line_arr[0].parse().unwrap();
        let operation: u8 = line_arr[1].parse().unwrap();
        let operation: Operation = match operation {
            0 => Operation::Even,
            1 => Operation::Prime,
            2 => Operation::Square,
            3 => Operation::Cube,
            _ => {
                continue;
            }
        };
        if operation == Operation::Even {
            if is_even!(value) {
                println!("{}", value + 2);
            } else {
                println!("{}", value + 1);
            }
        } else if operation == Operation::Prime {
            let mut i = value + 1;
            while !is_prime(i) {
                i += 1;
            }
            println!("{}", i);
        } else if operation == Operation::Square {
            let sqrt = ((value as f32).sqrt()) as usize + 1;
            println!("{}", sqrt * sqrt);
        } else if operation == Operation::Cube {
            let cbrt = ((value as f32).cbrt()) as usize + 1;
            println!("{}", cbrt.pow(3));
        }
    }
}
