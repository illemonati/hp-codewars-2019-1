import readline from 'readline';
const main = async () => {
    const rl = readline.createInterface({
        input: process.stdin,
    });
    const name = await getLine(rl);
    rl.close();
    console.log(`While we seem to disagree on this issue, ${name}, I respect your opinion and look forward to further discussion!`);
};
const getLine = (rl) => {
    return new Promise((resolve) => {
        rl.on('line', (line) => resolve(line));
    });
};
main().then();
