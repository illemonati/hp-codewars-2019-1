import readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
});

const getLine = (() => {
    const getLineGen = (async function* () {
        for await (const line of rl) {
            yield line;
        }
    })();
    return async () => (await getLineGen.next()).value;
})();

const main = async () => {
    const height = parseInt((await getLine()) as string);
    let sum = 0;
    rl.close();
    for (let i = 1; i <= height; ++i) {
        sum += i ** 2;
    }
    console.log(sum);
};

main().then();
