#[derive(Debug, Copy, Clone)]
enum Choice {
    Rock,
    Paper,
    Scissors,
    Lizard,
    Spock,
}

impl Choice {
    fn from_str(s: &str) -> Option<Self> {
        let res = match s.to_ascii_lowercase().as_str() {
            "rock" => Self::Rock,
            "paper" => Self::Paper,
            "scissors" => Self::Scissors,
            "lizard" => Self::Lizard,
            "Spock" => Self::Spock,
            _ => {
                return None;
            }
        };
        Some(res)
    }
    fn fight(&self, other: Choice) -> &'static str {
        match self {
            Self::Scissors => match other {
                Self::Paper => "cuts",
                Self::Lizard => "decapitates",
                _ => "",
            },
            Self::Paper => match other {
                Self::Rock => "covers",
                Self::Spock => "disproves",
                _ => "",
            },
            Self::Rock => match other {
                Self::Lizard => "crushes",
                Self::Scissors => "crushes",
                _ => "",
            },
            Self::Lizard => match other {
                Self::Spock => "poisons",
                Self::Paper => "eats",
                _ => "",
            },
            Self::Spock => match other {
                Self::Scissors => "smashes",
                Self::Rock => "vaporizes",
                _ => "",
            },
        }
    }
}

fn main() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("Oh Nose");
    let choices: Vec<Choice> = input
        .split_ascii_whitespace()
        .map(|c| Choice::from_str(c).unwrap())
        .collect();
    for i in 0..2 {
        let fight_res = choices[i].fight(choices[1 - i]);
        if !fight_res.is_empty() {
            let term = if i == 0 { "WINS" } else { "LOSES" };
            println!(
                "{} {}, {:?} {} {:?}",
                format!("{:?}", choices[0]).to_ascii_uppercase(),
                term,
                choices[i],
                fight_res,
                choices[1 - i]
            );
            return;
        }
    }
    println!(
        "Tie, {} does not effect {}",
        format!("{:?}", choices[0]).to_ascii_uppercase(),
        format!("{:?}", choices[0]).to_ascii_uppercase()
    )
}
