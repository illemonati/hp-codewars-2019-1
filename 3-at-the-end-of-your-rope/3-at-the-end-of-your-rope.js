import readline from 'readline';
const main = async () => {
    const input = await getLine();
    const radius = parseInt(input);
    const problemPi = 3.14159;
    const area = problemPi * radius ** 2;
    console.log(((area / 4) * 3).toFixed(2));
};
const getLine = () => {
    const rl = readline.createInterface({
        input: process.stdin,
    });
    return new Promise((resolve) => {
        rl.on('line', (line) => {
            rl.close();
            resolve(line);
        });
    });
};
main().then();
