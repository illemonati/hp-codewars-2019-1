import readline, { ReadLine } from 'readline';

const main = async () => {
    const amount = parseInt((await getLine()) as string);
    for (let i = 0; i < amount; i++) {
        const taxRate = Number(await getLine()) * 0.01;
        const total = Number(await getLine());
        const pretaxPrice = total / (1 + taxRate);
        const tax = pretaxPrice * taxRate;
        //prettier-ignore
        console.log(`On your ${total.toFixed(2)} purchase, the tax amount was \$${tax.toFixed(2)}`);
    }
};

const rl = readline.createInterface({
    input: process.stdin,
});

const getLine = (() => {
    const getLineGen = (async function* () {
        for await (const line of rl) {
            yield line;
        }
    })();
    return async () => (await getLineGen.next()).value;
})();

main().then();
