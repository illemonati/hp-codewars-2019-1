import readline from 'readline';
const rl = readline.createInterface({
    input: process.stdin,
});
const getLine = (() => {
    const getLineGen = (async function* () {
        for await (const line of rl) {
            yield line;
        }
    })();
    return async () => (await getLineGen.next()).value;
})();
const main = async () => {
    const amount = parseInt((await getLine()));
    rl.close();
    const totalPeople = amount * 2;
    const interactPeople = totalPeople - 2;
    console.log(amount * interactPeople);
};
main().then();
