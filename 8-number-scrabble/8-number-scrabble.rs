const SPELLINGS: [&str; 13] = [
    "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN",
    "ELEVEN", "TWELVE",
];

fn main() {
    let mut input_line = String::new();
    std::io::stdin()
        .read_line(&mut input_line)
        .expect("Big Boomer");
    let mut numbers: Vec<u8> = input_line
        .split_ascii_whitespace()
        .map(|n| n.parse::<u8>().unwrap_or(99))
        .collect();
    numbers.pop();
    let mut max_array = [0usize; 26];
    for number in &numbers {
        let letters_needed = build_letters_needed(*number);
        for i in 0..26 {
            if max_array[i] < letters_needed[i] {
                max_array[i] = letters_needed[i]
            }
        }
    }
    for i in 0..numbers.len() - 2 {
        print!("{} ", numbers[i])
    }
    print!("{}. ", numbers[numbers.len() - 1]);
    for (i, n) in max_array.iter().enumerate() {
        for _ in 0..*n {
            print!("{} ", (i + 65) as u8 as char)
        }
    }
    println!();
}

fn build_letters_needed(number: u8) -> [usize; 26] {
    let mut res = [0usize; 26];
    let spelling: &str = SPELLINGS[number as usize];
    for c in spelling.chars() {
        res[(c as u8 - 65) as usize] += 1;
    }
    res
}
